This project is a set of `Dockerfile` and `docker-compose.yml` used to setup a
GitLab.com production-like database environment to be used for local GitLab
development.

In particular it is trying to mirror the deployment used for Patroni, Consul,
Postgres, PGBouncer which is described in detail at
https://gitlab.com/gitlab-com/runbooks/-/blob/master/docs/pgbouncer/patroni-consul-postgres-pgbouncer-interactions.md
.

## Usage

First build the docker images:

Build pgbouncer image:

```sh
docker build -t gl-pgbouncer -f pgbouncer/Dockerfile .
```

Build patroni image:

```sh
docker build -t gl-patroni -f patroni/Dockerfile .
```

Start the database clusters:

```sh
docker-compose up
```

This forwards 2 ports to connect to the different Database clusters which you can then configure in `config/gitlab/database.yml`:

1. `main`: `http://127.0.0.1:11432`
1. `ci`: `http://127.0.0.1:12432`

## Details around replication for CI migration

At present the `ci` database is configured as a [Patroni standby cluster](https://patroni.readthedocs.io/en/latest/replica_bootstrap.html#standby-cluster) in `patroni/patroni-ci.yml` (also copied into `gl-patroni` image at `/patroni-ci.yml`). Additionally the CI PGBouncer write host is just pointing to the `main` database primary so that writes all go to the same primary.

See also `scripts/failover-test.rb` for how to test the migration process
described at
https://about.gitlab.com/handbook/engineering/development/enablement/sharding/migrate-ci-tables-to-new-database-plan.html
.
