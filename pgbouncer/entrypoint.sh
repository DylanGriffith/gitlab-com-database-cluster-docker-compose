export CONSUL_BIND_INTERFACE=eth0
# Sleep so that consul1 has time to start up otherwise it breaks
consul agent -retry-join=consul1 -data-dir /consul/data -dns-port 53 &

pgbouncer /pgbouncer.ini -u nobody
