export CONSUL_BIND_INTERFACE=eth0
sudo consul agent -dns-port 53 -config-file $CONSUL_CONFIG_FILE | grep -v 'Check is now critical.*patroni' &

pgbouncer /pgbouncer.ini &

# Important: without this the node advertises it's IP to other nodes
# as 127.0.0.1 and they all fail to connect to the leader during pg_basebackup
export PATRONI_RESTAPI_CONNECT_ADDRESS="$(hostname):8008"
export PATRONI_POSTGRESQL_CONNECT_ADDRESS="$(hostname):5432"

export PATRONI_NAME=$(hostname)
patroni $PATRONI_CONFIG_FILE
