#!/usr/bin/env ruby
require 'time'

def log_result(key, result, extra, allow_failure: false)
  symbol = result ? "✅" : (allow_failure ? "⚠️" : "❌")

  puts "#{Time.now.iso8601} #{symbol} #{key} - #{extra}"
end

def dns_validate(domain, allow_failure: false)
  result = `dig +short @127.0.0.1 -p 18600 #{domain} A`

  valid = result && result.start_with?("172.")

  log_result("DNS", valid, domain, allow_failure: allow_failure)
end

while true do
  dns_validate("master.patroni.service.consul")
  dns_validate("replica.patroni.service.consul")
  dns_validate("master-ci.patroni.service.consul", allow_failure: true)
  dns_validate("standby-leader-ci.patroni.service.consul", allow_failure: true)
  dns_validate("replica-ci.patroni.service.consul")

  sleep 1
end
