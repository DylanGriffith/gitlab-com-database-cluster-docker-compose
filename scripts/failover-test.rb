#!/usr/bin/env ruby

require 'pg'

STANDBY_REPLICATION_SLOT_NAME = 'patroni_standby_slot'

def pg_connect(port, dbname: 'postgres')
  PG.connect(host: '127.0.0.1', port: port, user: 'postgres', password: 'postgres', dbname: dbname)
end

main_pgbouncer = pg_connect(11432)
main_pgbouncer_management = pg_connect(11432, dbname: 'pgbouncer')
ci_pgbouncer_management = pg_connect(12432, dbname: 'pgbouncer')

# Pause CI pgbouncer
result = ci_pgbouncer_management.exec('PAUSE');

# Obtain LSN position of main
current_lsn = main_pgbouncer.exec('SELECT pg_current_wal_lsn()')[0]['pg_current_wal_lsn']
puts "Current LSN: #{current_lsn}"

# Wait until CI primary reaches this LSN position
while true
  up_to_date = main_pgbouncer.exec("SELECT restart_lsn >= '#{current_lsn}' as up_to_date from pg_replication_slots where slot_name = '#{STANDBY_REPLICATION_SLOT_NAME}'")[0]['up_to_date']
  puts "Checking if up to date: #{up_to_date}"
  break if up_to_date
  sleep 1
end

# Remove standby_cluster and trigger failover
`curl -s -XPATCH -d '{"standby_cluster": null}' http://localhost:9208/config`

# Change pgbouncer host
`sudo docker exec pgbouncer-ci cp /pgbouncer-ci.ini pgbouncer.ini`
result = ci_pgbouncer_management.exec('RELOAD');

# Unpause pgbouncer
result = ci_pgbouncer_management.exec('RESUME');
